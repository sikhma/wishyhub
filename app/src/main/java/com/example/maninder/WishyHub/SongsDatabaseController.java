package com.example.maninder.WishyHub;

import java.util.ArrayList;

/**
 * Created by root on 2/19/17.
 */

public class SongsDatabaseController {
    public static final int DatabaseVersion= 1;
    public static final String DataBase_Name = "wishyhub.db";
    public static final String TableName = "songdata";
    public static final String column1 = "uri";
    public static final String column2 = "name";
    public static final String column3 = "length ";
    public static final String column4 = "size";
    public static final String column5 = "country";
    public static final String column6 = "longitude";
    public static final String column7 = "latitude";
    public static final String textType = " Text ";
    public static final String comma = " , ";
    public ArrayList<String> report =  new ArrayList<String>();
}
