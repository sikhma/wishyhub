package com.example.maninder.WishyHub;

import android.content.Context;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;

import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;

import android.widget.TextView;

import java.util.ArrayList;


public class Main_Navigation_Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;

    ArrayAdapter<String> song_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__navigation_);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        getSupportActionBar().setDisplayShowTitleEnabled(false);


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        /**
         * Lets inflate the very first fragment
         * Here , we are inflating the TabFragment as the first Fragment
         */

        mFragmentManager = getSupportFragmentManager();
        mFragmentTransaction = mFragmentManager.beginTransaction();
        mFragmentTransaction.replace(R.id.containerView,new Tab_activity()).commit();

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main__navigation_, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();



        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        if (id == R.id.Signin_item) {
            // Handle the camera action

            fragmentTransaction.replace(R.id.containerView,new SigninFragment()).addToBackStack(null).commit(); // addtoBackStack function for when press back button it comes to previous screen

           // startActivity(new Intent(this, Signin_Activity.class));
        } else if (id == R.id.nav_gallery) {

            fragmentTransaction.replace(R.id.containerView,new BlankFragment()).addToBackStack(null).commit(); // addtoBackStack function for when press back button it comes to previous screen

        } else if (id == R.id.nav_slideshow) {

          //  FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.containerView,new ListView_fragment()).addToBackStack(null).commit();// addtoBackStack function for when press back button it comes to previous screen

        } else if (id == R.id.id_show_report) {

           // fragmentTransaction.replace(R.id.containerView,new Tab_activity()).addToBackStack(null).commit(); // addtoBackStack function for when press back button it comes to previous screen

            show_full_report_Activity();
        } else if (id == R.id.sell_stuff) {

            fragmentTransaction.replace(R.id.containerView,new postItem()).addToBackStack(null).commit(); // addtoBackStack function for when press back button it comes to previous screen



        } else if (id == R.id.menu_nav_reminder) {
            fragmentTransaction.replace(R.id.containerView,new Frag_reminder()).addToBackStack(null).commit(); // addtoBackStack function for when press back button it comes to previous screen

        } else if (id == R.id.id_create_profile) {
            fragmentTransaction.replace(R.id.containerView,new Profile_builder()).addToBackStack(null).commit(); // addtoBackStack function for when press back button it comes to previous screen

        }
        else if (id == R.id.id_information) {
           // fragmentTransaction.replace(R.id.containerView,new Frag_reminder()).addToBackStack(null).commit(); // addtoBackStack function for when press back button it comes to previous screen
            export_database();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    public void export_database() {

        ExportImportDB exportImportDB = new ExportImportDB();
        try {
            exportImportDB.exportDB();
          //  Snackbar.make(v,check, Snackbar.LENGTH_LONG).show();
        } catch (Exception e) {
           // Snackbar.make(v,e.getMessage(), Snackbar.LENGTH_LONG).show();
        }
      //  Snackbar.make(v,check, Snackbar.LENGTH_LONG).show();
    }
    public void show_full_report_Activity() {

        String report = synchronyzeReport();

        Intent mIntent = new Intent(this, FullReport.class);
        mIntent.putExtra("report", report);

        startActivity(mIntent);

    }

    /**
     * This funtion is gathering full report from database class and put in list
     */
    public String synchronyzeReport() {
        DatabaseController db = new DatabaseController(getApplicationContext());
        String report = null;
        ArrayList<String> list = new ArrayList<String>();
        list = db.getReport();

        if(list != null) {
            for (int i = 0; i < list.size(); i++) {
                report = report + list.get(i).toString() + "\n";
            }
        } else {
            report = "list is null";
        }
        return report;

    }


    public void show_cordinates(View v) {
        TextView textView_coardinate = null;
        Button coardinate_button = null;
        String location_data = null;

        String longitude, latitude;

        LocationManager locationManager;

        textView_coardinate = (TextView) findViewById(R.id.textview_cordinates_tab2);

        location_data  = "unknown";

        // Acquire a reference to the system Location Manager
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

// Define a listener that responds to location updates
        LocationListener locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                // Called when a new location is found by the network location provider.
                // makeUseOfNewLocation(location);

                // location_data = Double.toString(location.getLatitude());
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {}

            public void onProviderEnabled(String provider) {}

            public void onProviderDisabled(String provider) {}
        };


        if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

            String locationProvider = LocationManager.NETWORK_PROVIDER;
            // locationProvider = LocationManager.GPS_PROVIDER;

            Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);

            locationProvider = LocationManager.NETWORK_PROVIDER;

            longitude = Double.toString(lastKnownLocation.getLongitude());
            latitude = Double.toString(lastKnownLocation.getLatitude());


            location_data = longitude + ", " + latitude;

            //location_data  = "known";
            textView_coardinate.setText(location_data);
        }



    }

}
