package com.example.maninder.WishyHub;

/**
 * Created by root on 2/16/17.
 */

public class Song {

    private String name;
    private String size;
    private String uri;
    private String length;


    public Song(String uri, String name, String size, String length){
        this.uri = uri;
        this.name = name;
        this.size = size;
        this.length = length;

    }

    public String getUri() {
        return  uri;
    }

    public String getlength() {
        return  length;
    }

    public String getName() {
        return  name;

    }

    public String getSize() {
        return  size;
    }
}
