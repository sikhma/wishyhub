package com.example.maninder.WishyHub;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import static android.widget.Toast.makeText;

/**
 * Created by root on 7/21/16.
 */

public class Map_Tab_Fragment extends Fragment implements View.OnClickListener, OnMapReadyCallback {

    private GoogleMap googleMap;
    private MapView mapView;
    private LatLng my_location = null;
    private EditText editText_search_city;

    private LatLng latLng;
    private String markerTitle, markerDescription;

    private DatabaseController db;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.layout_map_tab_fragment, container, false);

        Button search_tab3 = (Button) rootView.findViewById(R.id.button_search_tab_frag3);
        search_tab3.setOnClickListener(this);

        editText_search_city = (EditText) rootView.findViewById(R.id.editText_search_city);

        mapView = (MapView) rootView.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);

        // mapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapView.getMapAsync(this);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_search_tab_frag3:

                hideKeyboard(); // hide keybard

                onMapSearch();

                mapView.getMapAsync(this);
                break;
        }
    }


    public void onMapSearch() {

        String location = editText_search_city.getText().toString();
        List<Address> addressList = null;
        String check;
        String sourceLat = "'";
        String sourceLong = "";
        Map<String, String> google_map_destination_timing = new HashMap<String, String>();
        String timing_result = null;

        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);



        if (location != null && !location.equals("")) {
            Geocoder geocoder = new Geocoder(getActivity());
            try {
                addressList = geocoder.getFromLocationName(location, 1);

            } catch (IOException e) {
                e.printStackTrace();
            }

            if (addressList.isEmpty()) {
                showToast("No such address found");
            } else {


                Address address = addressList.get(0);  // add first result of search in address instance

                db = new DatabaseController(getActivity());

                check = db.insertDataInDatabase(location, address.getLocality(), address.getAdminArea(), address.getCountryCode(), Double.toString(address.getLongitude()) , Double.toString(address.getLatitude()));

                showToast(check);

                latLng = new LatLng(address.getLatitude(), address.getLongitude());

                if(googleMap.isMyLocationEnabled()) {

                }

                    if(ContextCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        //locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);

                        String locationProvider = LocationManager.NETWORK_PROVIDER;
                        locationProvider = LocationManager.GPS_PROVIDER;

                        Location lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);

                        //  locationProvider = LocationManager.NETWORK_PROVIDER;;

                        if(lastKnownLocation != null) {

                            String url = makeURL(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude(),address.getLatitude(), address.getLongitude());



                            try {
                                timing_result =  new RetrieveFeedTask().execute(url).get();


                                if(timing_result ==null) {
                                    showToast("no result from google map api");
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            } catch (ExecutionException e) {
                                e.printStackTrace();
                            }

                        } else {

                        }

                        //  makeURL(address.getLatitude(), address.getLongitude());
                }

                markerTitle = address.getLocality();
                if(timing_result == null) {
                    markerDescription = address.getAdminArea() + ", " + address.getCountryCode()
                            + "( " + address.getLatitude() + " , " + address.getLongitude() + " )";
                } else {
                    markerDescription = timing_result;
                }
            }

        } else {
            showToast("Enter location in search box");
        }
    }

    public void onMapReady(GoogleMap mMap) {
        googleMap = mMap;
        LatLng local_location;

        // For showing a move to my location button
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            showToast("turn on location permission! ");


        } else {
            googleMap.setMyLocationEnabled(true);

            // For dropping a marker at a point on the Map

            if(latLng == null) {
                local_location = new LatLng( 37.8043637,-122.2711137); // bydefault location Oakland
                markerTitle = "Oakland";
                markerDescription = "California, US (37.8043637,-122.2711137)";
            } else {
                local_location = latLng;
            }


            googleMap.clear();
            googleMap.addMarker(new MarkerOptions().position(local_location).title(markerTitle).snippet(markerDescription));

            // For zooming automatically to the location of the marker
            CameraPosition cameraPosition = new CameraPosition.Builder().target(local_location).zoom(12).build();
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }

    public void set_coardinates(LatLng loc) {
        // this is for set coardinates
        my_location = loc;
    }

    public void showToast(String msg) {
        Toast mapToast = makeText(getActivity(),msg, Toast.LENGTH_LONG);
        mapToast.show();
    }

    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }


    public String makeURL (double sourcelat, double sourcelog, double destlat, double destlog ){
        StringBuilder urlString = new StringBuilder();
        urlString.append("http://maps.googleapis.com/maps/api/directions/json");
        urlString.append("?origin=");// from
        urlString.append(Double.toString(sourcelat));
        urlString.append(",");
        urlString
                .append(Double.toString( sourcelog));
        urlString.append("&destination=");// to
        urlString
                .append(Double.toString( destlat));
        urlString.append(",");
        urlString.append(Double.toString( destlog));
        urlString.append("&sensor=false&mode=driving&alternatives=true");
        return urlString.toString();
    }

}

class RetrieveFeedTask extends AsyncTask<String, Void, String> {

    private Exception exception;
    String json = "";
    InputStream is = null;
    Map<String, String> global_googleMap_direction = new HashMap<String, String>();

    protected String doInBackground(String... urls) {
        try {

            // Making HTTP request
            try {
                // defaultHttpClient
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(urls[0]);

                HttpResponse httpResponse = httpClient.execute(httpPost);
                HttpEntity httpEntity = httpResponse.getEntity();
                is = httpEntity.getContent();

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        is, "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }

                json = sb.toString();

                Decode(json);

                is.close();
            } catch (Exception e) {
                Log.e("Buffer Error", "Error converting result " + e.toString());
                return e.getMessage();
            }
            Log.d("JSON_RUTA", json);

        } catch (Exception e) {
            this.exception = e;


        }

        return json;
    }

    protected void onPostExecute(String feed) {
        // TODO: check this.exception
        // TODO: do something with the feed
    }

    public Map getResponse() {
        return global_googleMap_direction;
    }

    public void Decode(String result) throws JSONException {


        String status = null;

        List<String> list = new ArrayList();

        // TODO decode the JSON CharSequence (result)
        // get the 'posts' section from the JSON string
        JSONObject posts = null;
        try {
            /*
            parsing string which got from google map api direction  and get distance and duration time of destination
             */

            JSONObject mainObject = new JSONObject(result);


            JSONObject object = mainObject.getJSONArray("routes").getJSONObject(0);

            JSONObject array = object.getJSONArray("legs").getJSONObject(0);

            JSONObject object_distance = array.getJSONObject("distance");
            JSONObject object_duration = array.getJSONObject("duration");


            String distance  = object_distance.getString("text");
            String duration  = object_duration.getString("text");

            json = distance + "," + duration;

            global_googleMap_direction.put("distance", distance);
            global_googleMap_direction.put("duration", duration);


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }
}