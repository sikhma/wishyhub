package com.example.maninder.WishyHub;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by maninder on 8/15/16.
 */

public class SigninFragment extends Fragment implements View.OnClickListener {

    private EditText usernameField,passwordField;
    private TextView status,role,method, error_status;
    private Button button;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_sing_in, container, false);


        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);


        usernameField = (EditText)getActivity().findViewById(R.id.username_input);
        passwordField = (EditText)getActivity().findViewById(R.id.password_register_input);
        Button signin_button = (Button) getActivity().findViewById(R.id.button_login_post_signin);

        status = (TextView)getActivity().findViewById(R.id.list_view_status);
        // role = (TextView)findViewById(R.id.role);
        method = (TextView)getActivity().findViewById(R.id.method);
        error_status = (TextView)getActivity().findViewById(R.id.error);

        signin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getActivity(),usernameField.getText().toString() + " " + passwordField.getText().toString() , Toast.LENGTH_LONG).show();
            }
        });



    }

    @Override
    public void onClick(View view) {


    }

    public void loginPost(View view){
        Toast.makeText(getActivity(),"hell yea", Toast.LENGTH_LONG).show();
    }
}
