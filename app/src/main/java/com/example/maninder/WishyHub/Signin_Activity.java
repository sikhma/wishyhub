package com.example.maninder.WishyHub;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Signin_Activity extends AppCompatActivity {

    private EditText usernameField,passwordField;
    private TextView status,role,method, error_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_in);

        usernameField = (EditText) findViewById(R.id.username_input);
        passwordField = (EditText) findViewById(R.id.password_register_input);

        status = (TextView) findViewById(R.id.list_view_status);
        // role = (TextView)findViewById(R.id.role);
        method = (TextView) findViewById(R.id.method);
        error_status = (TextView) findViewById(R.id.error);

        Button signin_button = (Button) findViewById(R.id.button_login_post_signin);

        signin_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginPost(view);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //  getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void login(View view){
        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();
        method.setText("Get Method");
      //  new Signin_controller(this,status,error_status,0).execute(username,password);

    }

    public void loginPost(View view){

        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();
        method.setText("Post Method");

      //  new Signin_controller(this,status,error_status, 1).execute(username,password);

        if(status.getText().toString().equals( "success" )) {

            show_welcome_screen_activity(view);

        }
    }


    public void show_welcome_screen_activity(View view) {
        Intent intent = new Intent(this, Main_Navigation_Activity.class);

        startActivity(intent);

    }

}