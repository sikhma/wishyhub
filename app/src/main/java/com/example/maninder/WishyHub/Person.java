package com.example.maninder.WishyHub;

/**
 * Created by maninder on 8/16/16.
 * this class have person name and status
 */

public class Person {

    private String name;
    private String status;

    public Person(String name, String sta){

        super();
        this.name = name;
        this.status = sta;

    }
    public Person( String sta){

        super();
        this.name = sta;

    }

    public String getName() {
        return  name;

    }

    public String getStatus() {
        return  status;
    }
}
