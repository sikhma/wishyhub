package com.example.maninder.WishyHub;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import static android.widget.Toast.makeText;

/**
 * A fragment with a Google +1 button.
 * Activities that contain this fragment must implement the
 * {@link TaskFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TaskFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TaskFragment extends Fragment implements  View.OnClickListener {
    ListView lv;
    private List<Person> myPerson = new ArrayList<Person>();
    private List<String> database;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list_view, container, false);

        lv = (ListView) rootView.findViewById(R.id.layout_list_view_fragment);

        populatePerson();

        DatabaseAdapter adapter = new DatabaseAdapter(getActivity(), R.id.content_list_view_activity, myPerson);

        lv.setAdapter(adapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // TODO Auto-generated method stub

              //  showToast("This is item "+ myPerson.get(position).getName());

                Bundle bundle = new Bundle();
                bundle.putString("edttext", myPerson.get(position).getName());

                list_item_Fragment_detail fragobj = new list_item_Fragment_detail();
                fragobj.setArguments(bundle);

               getFragmentManager().beginTransaction().replace(R.id.containerView,fragobj).commit();

               // getFragmentManager().beginTransaction().add(((ViewGroup)getView().getParent()).getId(), fragobj).commit();

               // fragmentTransaction.replace(R.id.containerView,new Tab_activity()).commit();

            }
        });

        return rootView;
    }


    public void showToast(String msg) {
        Toast mapToast = makeText(getActivity(),msg, Toast.LENGTH_SHORT);
        mapToast.show();
    }

    private void populatePerson() {
        myPerson.add(new Person("Maninder", "online"));
        myPerson.add(new Person("Sam", "ofline"));
        myPerson.add(new Person("Raveena", "online"));
        myPerson.add(new Person("Satinder", "Available"));
        myPerson.add(new Person("Kara", "online"));
        myPerson.add(new Person("Sameer", "ofline"));
        myPerson.add(new Person("Ravi", "online"));
        myPerson.add(new Person("Romy", "Available"));
        myPerson.add(new Person("Chawala", "online"));
        myPerson.add(new Person("Smith", "ofline"));
        myPerson.add(new Person("Cody", "online"));
        myPerson.add(new Person("Aman", "Available"));
    }
    @Override
    public void onClick(View v) {

    }

    public void Transfer_database_to_list() {
        SQLiteDatabase db;


    }
}

