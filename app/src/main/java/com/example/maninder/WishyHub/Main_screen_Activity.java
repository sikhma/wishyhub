package com.example.maninder.WishyHub;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.plus.model.people.*;
import com.google.android.gms.plus.model.people.Person;

import java.util.ArrayList;

import static android.widget.Toast.makeText;


public class Main_screen_Activity extends AppCompatActivity implements View.OnClickListener{

    String check;
    DatabaseController db;
    private String report = " ";
    private EditText usernameField,passwordField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        hide_statusbar();
        setContentView(R.layout.activity_main_screen_);

        final Button signin = (Button) findViewById(R.id.Sign_in_button);
      //  final Button register = (Button) findViewById(R.id.register_button);
        usernameField = (EditText) findViewById(R.id.id_email_sign_in);
        passwordField = (EditText) findViewById(R.id.id_signin_password);
        final TextView textView_create_account = (TextView) findViewById(R.id.id_create_account);
        final TextView textView_skip = (TextView) findViewById(R.id.id_skip_main_screen);


        textView_create_account.setOnClickListener(this);
        textView_skip.setOnClickListener(this);

        createDatabase(); // creating data base for map data storage

        signin.setOnClickListener(this);
       // register.setOnClickListener(this);

        setupUI(findViewById(R.id.id_sign_in_layout));


    }

    public void show_ads() {
        // MobileAds.initialize(getApplicationContext(), "ca-app-pub-5105029831068562/3994989536");

        /*
        This is show for ads

        AdView adView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("testing").build(); // this is add showing on front page
        adView.loadAd(adRequest);*/
    }

    public void setupUI(View view) {

        // Set up touch listener for non-text box views to hide keyboard.
        if (!(view instanceof EditText)) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    hideSoftKeyboard(Main_screen_Activity.this);
                    return false;
                }
            });
        }

        //If a layout container, iterate over children and seed recursion.
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                View innerView = ((ViewGroup) view).getChildAt(i);
                setupUI(innerView);
            }
        }
    }
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }


    public void main_layout_click(View v) {
        showToast("hello main layout");
    }

    public void hide_keyboard() {

        try {
            InputMethodManager input = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

            input.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }
    public void hide_statusbar() {
        // This example uses decor view, but you can use any visible view.
        View decorView = this.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

        public void export_database(View v ) {

        ExportImportDB exportImportDB = new ExportImportDB();
        try {
            check = exportImportDB.exportDB();
            Snackbar.make(v,check, Snackbar.LENGTH_LONG).show();
        } catch (Exception e) {
            Snackbar.make(v,e.getMessage(), Snackbar.LENGTH_LONG).show();
        }
        Snackbar.make(v,check, Snackbar.LENGTH_LONG).show();
    }

    public void show_welcome_screen_activity(View view) {

        startActivity(new Intent(this, Main_Navigation_Activity.class));

    }


    public void open_sign_in_activity(View view) {

            Intent intent = new Intent(this, Signin_Activity.class);
            startActivity(intent);

    }

    public void open_register_activity(View view) {

        Intent intent = new Intent(this, RegisterActivity.class);

        startActivity(intent);

    }



    public void createDatabase() {

        try {
        db = new DatabaseController(getApplicationContext()); // creating table

        // cond = db.checkDatabase();
        //    showToast("table created ");

        } catch (SQLiteException e){
            showToast(e.getMessage());
        }
    }

    public void showToast(String msg) {
        Toast mapToast = makeText(this,msg, Toast.LENGTH_LONG);
        mapToast.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.Sign_in_button:
               // loginPost(view);
                Intent intent = new Intent(this, FingerPrint.class);

                startActivity(intent);
                break;
            case R.id.id_create_account:
                open_register_activity(view);
                break;
            case R.id.id_skip_main_screen:
                show_welcome_screen_activity(view);
                break;
        }
    }

    public void loginPost(View view){

        String username = usernameField.getText().toString();
        String password = passwordField.getText().toString();

        String status = "";
        String error_status = "";
        new Signin_controller(this,status,error_status, 1).execute(username,password);

        if(status.equals( "success" )) {

            show_welcome_screen_activity(view);

        } else {
            Snackbar.make(view,"Server down", Snackbar.LENGTH_LONG).show();
        }

    }
}
