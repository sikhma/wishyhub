package com.example.maninder.WishyHub;

/**
 * Created by root on 7/21/16.
 */


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class BlankFragment extends Fragment {

    private ListView listView;
    private List<Person> myPerson = new ArrayList<Person>();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_blank, container, false);

        listView = (ListView) rootView.findViewById(R.id.list_view_id);


        populatePerson();

        MovieAdapter adapter = new MovieAdapter(getActivity(), R.id.content_list_view_activity, myPerson);

        listView.setAdapter(adapter);
        return rootView;
    }

    private void populatePerson() {
        myPerson.add(new Person("Maninder", "online"));
        myPerson.add(new Person("Sam", "ofline"));
        myPerson.add(new Person("Raveena", "online"));
        myPerson.add(new Person("Satinder", "Available"));
        myPerson.add(new Person("Maninder", "online"));
        myPerson.add(new Person("Sam", "ofline"));
        myPerson.add(new Person("Raveena", "online"));
        myPerson.add(new Person("Satinder", "Available"));
        myPerson.add(new Person("Maninder", "online"));
        myPerson.add(new Person("Sam", "ofline"));
        myPerson.add(new Person("Raveena", "online"));
        myPerson.add(new Person("Satinder", "Available"));
    }

}
