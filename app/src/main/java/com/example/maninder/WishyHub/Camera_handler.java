package com.example.maninder.WishyHub;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.ContextMenu;
import android.view.View;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

/**
 * Created by root on 2/11/17.
 */

public class Camera_handler extends Activity {

    private Uri global_image_uri;
    private static final int RESULT_LOAD_IMAGE = 1;
    private static final int REQUEST_TAKE_PHOTO = 2;
    private Uri image_uri;
    Context context;

   public void setContext(Context context) {
       this.context = context;
   }

    public void dispatchTakePictureIntent() {
        Uri photoURI;

        Intent takePictureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {

                photoURI = Uri.fromFile(photoFile);

                global_image_uri = photoURI;

                takePictureIntent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, photoURI);
                //  takePictureIntent.putExtra("return-data", true);
                // startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);

              //  context.startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);

            }
        } else {

            show_toast("packet manager is null");

        }
    }

    public void activeGallery() {

         Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(intent, RESULT_LOAD_IMAGE);
        //  global_image_uri = intent.getData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Uri selectedImage = null;


        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case RESULT_LOAD_IMAGE: // this is case or select from gallery

                    if (data != null) {
                        global_image_uri = data.getData();
                    }


                    if (data != null) {

                        image_uri = data.getData();
                    }


                    if (selectedImage != null) {
                      //  imageView.setImageURI(selectedImage);
                    }


                    break;
                case REQUEST_TAKE_PHOTO: // this is case for take picture from camera

                    try {

                        if (global_image_uri != null) {

                            //  picturePath = get_path_from_uri(global_image_uri);

                          //  imageView.setImageURI(global_image_uri);
                            image_uri = global_image_uri;

                            //  imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));

                        } else {

                            throw new CustomException("This is exception from postitem.java from case REQUEST_IMAGE_CAPTURE in onActivityResult() ");
                        }
                    } catch (CustomException m) {

                    }

                    break;
            }
        }
    }

    public Uri get_image_uri() {
        Uri uri = image_uri;

        return uri;
    }
    public void show_toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public File createImageFile() throws IOException {
        // Create an image file name
        String date = (DateFormat.format("dd-MM-yyyy hh:mm:ss", new java.util.Date()).toString());

        String imageFileName = date + "_";

        File directory = new File(Environment.getExternalStorageDirectory() + "/wishyhub");

        if (!directory.exists()) {
            directory.mkdirs();   // make directory
        }

        //  File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        // storageDir = directory;


        // Save a file: path for use with ACTION_VIEW intents
        //  mCurrentPhotoPath = "file:" + image.getAbsolutePath();

        // image = new File(directory,);
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/wishyhub", "WH_" + date + ".jpg");
        //return new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/wishyhub");
    }

    public String get_path_from_uri(Uri uri) {

        String path = null;
        Cursor cursor;
        String[] filePathColumn = {MediaStore.Images.Media.DATA};
        int columnIndex;

        cursor = this.getContentResolver().query(uri,
                filePathColumn, null, null, null);
        cursor.moveToFirst();

        columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        path = cursor.getString(columnIndex);
        cursor.close();

        return path;
    }

    public Bitmap image_compress(String miFoto ) {

        Bitmap bmp = BitmapFactory.decodeFile(miFoto);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 70, bos);

        return bmp;

        //This is for  send  image over http
        // InputStream in = new ByteArrayInputStream(bos.toByteArray());
        //ContentBody foto = new InputStreamBody(in, "image/jpeg", "filename");
    }
}
