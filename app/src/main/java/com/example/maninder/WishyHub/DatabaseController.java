package com.example.maninder.WishyHub;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

/**
 * Created by root on 9/16/16.
 */

public class DatabaseController extends SQLiteOpenHelper {


    public static final int DatabaseVersion= 1;
    public static final String DataBase_Name = "wishyhub.db";
    public static final String TableName = "mapdata";
    public static final String column1 = "id";
    public static final String column2 = "name";
    public static final String column3 = "city ";
    public static final String column4 = "state";
    public static final String column5 = "country";
    public static final String column6 = "longitude";
    public static final String column7 = "latitude";
    public static final String textType = " Text ";
    public static final String comma = " , ";
    public ArrayList<String> report =  new ArrayList<String>();

    public DatabaseController(Context context) {
        super(context, DataBase_Name, null, DatabaseVersion);
    }



    @Override
    public void onCreate(SQLiteDatabase db) {

        String message;

        try {
            db.execSQL(create_Table_sql_query());
            report.add("from on Create method in DataBaseController , create table " + TableName + "executed");
        } catch (Exception s) {

            message =  s.getMessage().toString();

            report.add("from on Create method in DataBaseController Exception thrown : "+ message);
        }


    }



    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


        try {

            db.execSQL(deleteTable());
            report.add("from on Upgrade method in DataBaseController , delet table " + TableName + "executed");
        } catch(SQLException e) {
            report.add("from on Upgrade method in DataBaseController Eception : " + e.getMessage() );
        }

        onCreate(db);
        report.add("from on Create method in DataBaseController , create table " + TableName + "executed");
    }

    public String create_Table_sql_query() {
        String sql = null;

        sql = "CREATE TABLE " + TableName + " (" + column1 + " INTEGER PRIMARY KEY AUTOINCREMENT ," + column2 + textType +comma+ column3 + textType +comma+ column4 + textType+comma + column5 + textType +comma+ column6 + textType +comma+ column7 + textType + ")";
        return sql;
    }

    public String deleteTable() {
        String sql = null;

        sql = "DROP TABLE IF EXISTS " + TableName;
        return sql;
    }

    public String insertDataInDatabase(String name, String city, String state, String country, String longitude , String latitude) {


        SQLiteDatabase db = getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        long newRowId = 00000000; // just value for check if data inserted in table or not

        contentValues.put(column2, name);
        contentValues.put(column3, city);
        contentValues.put(column4, state);
        contentValues.put(column5, country);
        contentValues.put(column6, longitude);
        contentValues.put(column7, latitude);

     try {
         db.insert(TableName, null, contentValues);

        return "data inserted in " + TableName;
    } catch (SQLException e) {
        return e.getMessage();
    }

    }

    public String checkDatabase() {

        SQLiteDatabase db = getReadableDatabase();
        try {
            db.rawQuery("SELECT name FROM " + DataBase_Name + " WHERE type='table' " , null);
            return "true";
        } catch (SQLException e) {
            return e.getMessage();
        }
    }

    public void getData(){
         ArrayList<String> localreport =  new ArrayList<String>();
        SQLiteDatabase db = this.getReadableDatabase();

        String sql =  "select * from "+ "wishyhub.db "+TableName;
        sql = "SELECT name FROM " + "wishyhub.db" + " WHERE type='table' "; //eroor
        sql = "SELECT name FROM " + "sqlite_master" + " WHERE type='table' ";
        sql =  "select * from "+TableName;
       // sql =  ".tables";
        Cursor c =  db.rawQuery(sql, null );


        if (c.moveToFirst())
        {
            do{
                localreport.add(c.getString(0) + " , " + c.getString(1) +" , "+ c.getString(2)+" , "+ c.getString(3)+" , "+ c.getString(4)+" , "+ c.getString(5)+" , "+ c.getString(6));

            }while (c.moveToNext());
        }
        if (localreport.size() >= 0)
        {
            for (int i=0; i<localreport.size(); i++)
            {
                report.add( localreport.get(i).toString() + "");

            }

        }
    }

    public String cleantDatabase() {
        SQLiteDatabase db = getWritableDatabase();
        try {
            db.execSQL("delete from " + TableName);
            return "true";
        } catch (SQLException e) {
            return e.getMessage();
        }
    }

    public ArrayList  getReport() {

        getData();

       // report.add(checkDatabase());

        report.add("\n\n report end here\n\n");
        report.add("==============================================" +
                "==============================");

        return report;
    }
}
