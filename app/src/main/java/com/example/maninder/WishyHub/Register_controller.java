package com.example.maninder.WishyHub;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;

/**
 * Created by root on 7/2/16.
 */

public class Register_controller  extends AsyncTask<String,Void,String> {

    String err = "error";
    String status = "";
    TextView status_controller;
    ArrayList<String> list = new ArrayList<>(2);
    private Context context;

    private String domainname = "http://www.wishyhub.com";
    private Boolean alert_diolog = false;

    public Register_controller( Context context, TextView status) {
        this.status_controller = status;
        this.context = context;
    }

    @Override
    protected void onPreExecute(){

    }

    @Override
    protected String doInBackground(String... arg0)  {

        try {
        String username = arg0[0];
        String password = arg0[1];


        String link = domainname + "/project/Register.php";

        String data = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8");
        data += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");

        URL url = new URL(link);
        URLConnection conn = url.openConnection();

        conn.setDoOutput(true);
        OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

        wr.write(data);
        wr.flush();

        BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

        StringBuilder sb = new StringBuilder();
        String line = null;

        // Read Server Response
        while ((line = reader.readLine()) != null) {
            sb.append(line);
            break;
        }

        err = sb.toString();
        // return sb.toString();



            Decode(sb);
        } catch (Exception e) {
            err = e.getMessage();
        }


        // return new String("Exception: " + e.getMessage());
        return "";

}


    public  void Decode(CharSequence result) throws JSONException {
        // TODO decode the JSON CharSequence (result)
        // get the 'posts' section from the JSON string
        JSONObject posts = null;
        try {
            posts = new JSONObject(result.toString()).getJSONObject("posts");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Get 'fname' & 'lname' from 'posts'
        //  Log.i(JSONParser.class.getName(),"username is:"+posts.getString("username"));
        //Log.i(JSONParser.class.getName(), "password is:" + posts.getString("password"));



        // status = " "+posts.getString("fname");

        if (posts == null) {
            status = " "+posts.getString("status");
        }

        Iterator<?> keys = posts.keys();  // use iterator to get json object


        while (keys.hasNext()) {
            String key = (String)keys.next();


            list.add(posts.get(key).toString());   // add item from json array in list first is error and 2nd is status

        }

        err = list.get(0);
        status = list.get(1);

    }
    @Override
    protected void onPostExecute(String s) {


        show_alert_box(status);


        }


    public void show_alert_box(String alert) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(alert);
        builder.setCancelable(true);

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                alert_diolog = true;

                dialog.dismiss();


                if (status.equals("user created successfully")) {

                    context.startActivity(new Intent(context, Signin_Activity.class));

                } else {


                }

            }
        });


        builder.create();
        builder.show();
    }
}
