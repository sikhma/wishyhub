package com.example.maninder.WishyHub;


import android.net.Uri;
import android.os.Environment;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.support.v4.app.Fragment;


/**
 * Created by root on 7/7/16.
 */

public class Universal_class  {

    Universal_class() {

    }

    public static String status_for_register_activity = "";
    public static String error_for_register_activity = "";

    public void set_status_for_register_activity(String status) {

        status_for_register_activity = status;
    }

    public void set_error_for_register_activity(String status) {

        status_for_register_activity = status;
    }

    public String get_status_for_register_activity() {
        return status_for_register_activity;
    }

    public String get_error_for_register_activity() {
        return error_for_register_activity;
    }

    public String createDirectory(String fileName, String foldername, String data) {
        File directory = new File(Environment.getExternalStorageDirectory() + "/"+foldername);

        if(!directory.exists()) {
            File imageDirectory = new File(foldername);

            imageDirectory.mkdirs();
        }

        File file = new File(directory, fileName);
        // file = new File(getFilesDir(), fileName);

        if (file.exists()) {
            file.delete();
        }

        try {
            FileOutputStream out = new FileOutputStream(file);

            byte datas[] = data.getBytes();

            out.write(datas);   // add data
            out.flush();   // write data
            out.close();  // closing file
        } catch (IOException e) {
            e.printStackTrace();
        }

        String imageurl = Environment.getExternalStorageDirectory().getAbsolutePath() + "/money.jpg";

        try {
            copyFileUsingFileStreams(new File(imageurl),  new File(Environment.getExternalStorageDirectory() + "/"+foldername, "myphoto.png"));
            copyDirectoryOneLocationToAnotherLocation(new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/pcm"), new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/wishyhub/pcm"));
        } catch (IOException e) {
            return e.getMessage();
        }

        return  "creted folder " + foldername;
    }

    private static void copyFileUsingFileStreams(File source, File dest)
            throws IOException {
        InputStream input = null;
        OutputStream output = null;
        try {
            input = new FileInputStream(source);
            output = new FileOutputStream(dest);
            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = input.read(buf)) > 0) {
                output.write(buf, 0, bytesRead);
            }
        } finally {
            input.close();
            output.close();
        }
    }


    public static void copyDirectoryOneLocationToAnotherLocation(File sourceLocation, File targetLocation)
            throws IOException {

        if (sourceLocation.isDirectory()) {
            if (!targetLocation.exists()) {
                targetLocation.mkdir();
            }

            String[] children = sourceLocation.list();
            for (int i = 0; i < sourceLocation.listFiles().length; i++) {

                copyDirectoryOneLocationToAnotherLocation(new File(sourceLocation, children[i]),
                        new File(targetLocation, children[i]));
            }
        } else {

            InputStream in = new FileInputStream(sourceLocation);

            OutputStream out = new FileOutputStream(targetLocation);

            // Copy the bits from instream to outstream
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();
        }

    }

    public static String convert_bit_to_mb(String duration) {
        String out = "";
        long dur = Long.parseLong(duration);

        long fileSizeInMB = (dur / 1024)/1024;

        out = String.valueOf(fileSizeInMB);


        return out + " mb";
    }

}


