package com.example.maninder.WishyHub;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class List_view_activity extends ActionBarActivity {

    private ListView listView;
    private List<Person> myPerson = new ArrayList<Person>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.fragment_list_view);

        listView = (ListView) findViewById(R.id.layout_list_view_fragment);

        populatePerson();

        MovieAdapter adapter = new MovieAdapter(this, R.id.content_list_view_activity, myPerson);

        listView.setAdapter(adapter);

    }

    private void populatePerson() {
        myPerson.add(new Person("Maninder", "online"));
        myPerson.add(new Person("Sam", "ofline"));
        myPerson.add(new Person("Raveena", "online"));
        myPerson.add(new Person("Satinder", "Available"));
        myPerson.add(new Person("Maninder", "online"));
        myPerson.add(new Person("Sam", "ofline"));
        myPerson.add(new Person("Raveena", "online"));
        myPerson.add(new Person("Satinder", "Available"));
        myPerson.add(new Person("Maninder", "online"));
        myPerson.add(new Person("Sam", "ofline"));
        myPerson.add(new Person("Raveena", "online"));
        myPerson.add(new Person("Satinder", "Available"));
    }

}
