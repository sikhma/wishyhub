package com.example.maninder.WishyHub;

import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 *
 */
public class ListView_fragment extends Fragment  {
    ListView list;
    TextView text;


    ListView lv;
    private List<Person> myPerson = new ArrayList<Person>();
    TextView textView_song_count;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list_view, container, false);

        lv = (ListView) rootView.findViewById(R.id.layout_list_view_fragment);
        //textView_song_count = (TextView) rootView.findViewById(R.id.id_song_count);

        populatePerson();

        MovieAdapter adapter = new MovieAdapter(getActivity(), R.id.content_list_view_activity, myPerson);

        lv.setAdapter(adapter);

        return rootView;
    }

    private void populatePerson() {
        int count = getMusic().length;
       // textView_song_count.setText(" Total song found" +Integer.toString(count));
        for(int i = 0; i < 30; i ++) {
            myPerson.add(new Person(getMusic()[i], ""));
        }

    }

    private String[] getMusic() {
        final Cursor mCursor = getActivity().managedQuery(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Audio.Media.DISPLAY_NAME }, null, null,
                "LOWER(" + MediaStore.Audio.Media.TITLE + ") ASC");

        int count = mCursor.getCount();

        String[] songs = new String[count];
        int i = 0;
        if (mCursor.moveToFirst()) {
            do {
                songs[i] = mCursor.getString(0);
                i++;
            } while (mCursor.moveToNext());
        }

        mCursor.close();

        return songs;
    }

    public void onBackPresed()  {

        FragmentManager fm = getActivity().getSupportFragmentManager();
        fm.popBackStack();

    }

}
