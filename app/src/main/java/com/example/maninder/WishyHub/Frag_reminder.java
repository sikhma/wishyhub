package com.example.maninder.WishyHub;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.widget.Toast.makeText;

public class Frag_reminder extends Fragment implements  View.OnClickListener{
    private ArrayList<Person> myPerson3 = new ArrayList<Person>();
    private RecyclerView.Adapter mAdapter3;
    RecyclerView mRecyclerView_3;
    Button button_additem;
    EditText editText_frag_add_item;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reminder, container, false);

        button_additem  = (Button) rootView.findViewById(R.id.button_frag_reminder);

        mRecyclerView_3 = (RecyclerView) rootView.findViewById(R.id.recycler_view_frag_reminder);
        editText_frag_add_item = (EditText) rootView.findViewById(R.id.editText_frag_reminder);
        mRecyclerView_3.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL,false));

        mAdapter3 = new CustomAdapter4(myPerson3, this);


        mRecyclerView_3.setAdapter(mAdapter3);
        button_additem.setOnClickListener(this);



        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_frag_reminder:
                myPerson3.add(new Person(editText_frag_add_item.getText().toString()));
                showToast("item added");
                hide_keyboard();
                editText_frag_add_item.getText().clear();
                mAdapter3.notifyDataSetChanged();

        }
    }

    public void hide_keyboard() {

        try {
            InputMethodManager input = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

            input.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }


    public void showToast(String msg) {
        Toast mapToast = makeText(getActivity(),msg, Toast.LENGTH_SHORT);
        mapToast.show();
    }


}



/**
 * Provide views to RecyclerView with data from mDataSet.
 * This Adapter is for first recyclerView
 */
class CustomAdapter4 extends RecyclerView.Adapter<CustomAdapter4.ViewHolder> {
    private static final String TAG = "CustomAdapter4";

    private ArrayList<Person> mDataSet = new ArrayList<Person>();
    Fragment fragment;

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)

    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder implements  View.OnTouchListener{

        private final TextView textView_status;

        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Element " + getPosition() + " clicked.");
                }
            });

            textView_status = (TextView) v.findViewById(R.id.textView_frag_reminder_item_detail);

            textView_status.setOnTouchListener(this);
        }



        public TextView getTextView_status() {
            return textView_status;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            return false;
        }
    }
    // END_INCLUDE(recyclerViewSampleViewHolder)

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    public CustomAdapter4(ArrayList<Person> dataSet, Fragment frag) {
        mDataSet = dataSet;
        this.fragment = frag;
    }

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.reminder_item_detail, viewGroup, false);


        return new ViewHolder(v);
    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");


        // Get element from your dataset at this position and replace the contents of the view
        // with that element
       // viewHolder.getTextView_name().setText(mDataSet.get(position).getName());
        viewHolder.getTextView_status().setText(mDataSet.get(position).getName());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "first recycler view ", Toast.LENGTH_SHORT).show();

                Fragment localfragment = new list_item_Fragment_detail();

                Bundle args = new Bundle();
                args.putString("data", mDataSet.get(position).getName());
                localfragment.setArguments(args);

                FragmentTransaction fragmentTransaction = fragment.getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.containerView, localfragment).addToBackStack(null).commit();

            }
        });

    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}

