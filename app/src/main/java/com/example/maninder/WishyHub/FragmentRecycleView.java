package com.example.maninder.WishyHub;

import android.app.SearchManager;
import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;
import static android.widget.Toast.makeText;


public class FragmentRecycleView extends Fragment implements  View.OnClickListener {

    private RecyclerView mRecyclerView1,mRecyclerView_2,mRecyclerView_3,mRecyclerView_4,mRecyclerView_5,mRecyclerView_6;
    public RecyclerView.Adapter mAdapter1, mAdapter2, mAdapter3,mAdapter4, mAdapter5, mAdapter6;
    private ArrayList<Song> mySong1 = new ArrayList<Song>();
    private ArrayList<Song> mySong2 = new ArrayList<Song>();
    private ArrayList<Song> mySong3 = new ArrayList<Song>();
    private ArrayList<Song> mySong4 = new ArrayList<Song>();
    private ArrayList<Song> mySong5 = new ArrayList<Song>();

    private ArrayList<Person> myPerson2 = new ArrayList<Person>();
    private ArrayList<Person> myPerson3 = new ArrayList<Person>();
    TextView  textView_song_count;
    SwipeRefreshLayout swipeRefreshLayout;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.recycle_view_list_layout, container, false);
        textView_song_count = (TextView) rootView.findViewById(R.id.id_count_song);


         swipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.swiperefresh);

        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        swipeRefreshLayout.setRefreshing(true);

                        populateSong();
                        populatePerson2();

                        swipeRefreshLayout.setRefreshing(false);
                    }


                }
        );


        mRecyclerView1 = (RecyclerView) rootView.findViewById(R.id.recycler_view_id_1);
        mRecyclerView_2 = (RecyclerView) rootView.findViewById(R.id.recycler_view_id_2);
        mRecyclerView_3 = (RecyclerView) rootView.findViewById(R.id.recycler_view_id_3);
        mRecyclerView_4 = (RecyclerView) rootView.findViewById(R.id.recycler_view_id_4);
        mRecyclerView_5 = (RecyclerView) rootView.findViewById(R.id.recycler_view_id_5);
        mRecyclerView_6 = (RecyclerView) rootView.findViewById(R.id.recycler_view_id_6);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        //mRecyclerView1.setHasFixedSize(true);

        // use a linear layout manager

        mRecyclerView1.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView_2.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView_3.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView_4.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView_5.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        mRecyclerView_6.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        // specify an adapter ()
        mAdapter1 = new CustomAdapter1(mySong1, this);
        mAdapter2 = new CustomAdapter2(myPerson2, this);
        mAdapter3 = new CustomAdapter1(mySong2, this);
        mAdapter4 = new CustomAdapter2(myPerson3, this);
        mAdapter5 = new CustomAdapter1(mySong3, this);
        mAdapter6 = new CustomAdapter1(mySong4, this);

        mRecyclerView1.setAdapter(mAdapter1);
        mRecyclerView_2.setAdapter(mAdapter2);
        mRecyclerView_3.setAdapter(mAdapter3);
        mRecyclerView_4.setAdapter(mAdapter4);
        mRecyclerView_5.setAdapter(mAdapter5);
        mRecyclerView_6.setAdapter(mAdapter6);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main__navigation_, menu);
        showToast("on create menu popup");

        MenuItem item = (MenuItem) menu.findItem(R.id.id_main_search_click).getActionView();

    }

    private void populateSong() {
        Song[] songs = getMusic();
        int count = songs.length;
         textView_song_count.setText(" Total song found " +Integer.toString(count));

        if(!mySong1.isEmpty()) {
            mySong1.clear();
        }
        if(!mySong2.isEmpty()) {
            mySong2.clear();
        }

        if(!mySong3.isEmpty()) {
            mySong3.clear();
        }
        if(!mySong4.isEmpty()) {
            mySong4.clear();
        }

        int size = count / 4;
        for(int i = 0; i < size; i ++) {
            mySong1.add(new Song(songs[i].getUri(), songs[i].getName(), songs[i].getSize(),songs[i].getlength()));

        }

        for(int i = size+1; i < size*2; i ++) {
            mySong2.add(new Song(songs[i].getUri(), songs[i].getName(), songs[i].getSize(),songs[i].getlength()));

        }

        for(int i = (size*2) + 1; i < size*3; i ++) {
            mySong3.add(new Song(songs[i].getUri(), songs[i].getName(), songs[i].getSize(),songs[i].getlength()));

        }

        for(int i = (size*3) + 1; i < count; i ++) {
            mySong4.add(new Song(songs[i].getUri(), songs[i].getName(), songs[i].getSize(),songs[i].getlength()));

        }


        mAdapter1.notifyDataSetChanged();
        mAdapter3.notifyDataSetChanged();
        mAdapter5.notifyDataSetChanged();
        mAdapter6.notifyDataSetChanged();

    }

    private  Song[]  getMusic() {
        final Cursor mCursor = getActivity().managedQuery(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Audio.Media.DATA, MediaStore.Audio.Media.DISPLAY_NAME,
                        MediaStore.Video.Media.SIZE,  MediaStore.Video.Media.DURATION}, null, null,
                "LOWER(" + MediaStore.Audio.Media.TITLE + ") ASC");

        int count = mCursor.getCount();


        Song[] songs = new Song[count];

        int i = 0;
        if (mCursor.moveToFirst()) {
            do {
                songs[i] = new Song(mCursor.getString(0),mCursor.getString(1),mCursor.getString(2),mCursor.getString(3));
                i++;
            } while (mCursor.moveToNext());
        }

        mCursor.close();

        return songs;
    }

    private void populatePerson2() {
        myPerson2.add(new Person("Maninderpal", "online"));
        myPerson2.add(new Person("Samantha", "ofline"));
        myPerson2.add(new Person("Raveena toor", "online"));
        myPerson2.add(new Person("Satinder singh", "Available"));
        myPerson2.add(new Person("Kara fat", "online"));
        myPerson2.add(new Person("Sameer ku", "ofline"));
        myPerson2.add(new Person("Ravi sha", "online"));
        myPerson2.add(new Person("Romy gill", "Available"));
        myPerson2.add(new Person("Chawalaar", "online"));
        myPerson2.add(new Person("Smithana", "ofline"));
        myPerson2.add(new Person("Cody Singh", "online"));
        myPerson2.add(new Person("Aman kaur", "Available"));

        myPerson3.add(new Person("Maninderpal", "online"));
        myPerson3.add(new Person("Sameer", "ofline"));
        myPerson3.add(new Person("sharveena", "online"));
        myPerson3.add(new Person("ratinder", "Available"));
        myPerson3.add(new Person("Sara", "online"));
        myPerson3.add(new Person("Manameer", "ofline"));
        myPerson3.add(new Person("kavi", "online"));
        myPerson3.add(new Person("tomy", "Available"));
        myPerson3.add(new Person("kawala", "online"));
        myPerson3.add(new Person("Smithan", "ofline"));
        myPerson3.add(new Person("podys", "online"));
        myPerson3.add(new Person("Amandeep", "Available"));
    }

    public void showToast(String msg) {
        Toast mapToast = makeText(getActivity(), msg, Toast.LENGTH_LONG);
        mapToast.show();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){


        }
    }

}

//--------------------------------------Custom Adapter1 Start---------------------------------------------------------------------------------------//

/**
 * Provide views to RecyclerView with data from mDataSet.
 * This Adapter is for first recyclerView
 */
 class CustomAdapter1 extends RecyclerView.Adapter<CustomAdapter1.ViewHolder> {
    private static final String TAG = "CustomAdapter1";

    private ArrayList<Song> mDataSet = new ArrayList<Song>();
    Fragment fragment;

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView_name;
        private final TextView textView_status;

        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Element " + getPosition() + " clicked.");
                }
            });
            textView_name = (TextView) v.findViewById(R.id.textview_name_id);
            textView_status = (TextView) v.findViewById(R.id.textView_status_id);
        }

        public TextView getTextView_name() {
            return textView_name;
        }

        public TextView getTextView_status() {
            return textView_status;
        }
    }
    // END_INCLUDE(recyclerViewSampleViewHolder)

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    public CustomAdapter1(ArrayList<Song> dataSet, Fragment frag) {
        mDataSet = dataSet;
        this.fragment = frag;
    }

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.cardview_for_recycle_view_fragment, viewGroup, false);


        return new ViewHolder(v);
    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");


        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        viewHolder.getTextView_name().setText(mDataSet.get(position).getName());
        viewHolder.getTextView_status().setText(Universal_class.convert_bit_to_mb(mDataSet.get(position).getSize()));

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),"Song playing ", Toast.LENGTH_SHORT).show();

                Fragment localfragment = new list_item_Fragment_detail();
                Bundle args = new Bundle();
                args.putString("uri",mDataSet.get(position).getUri());
                args.putString("name",mDataSet.get(position).getName());
                args.putString("size",mDataSet.get(position).getSize());
                args.putString("length",mDataSet.get(position).getlength());

                localfragment.setArguments(args);
                FragmentTransaction fragmentTransaction = fragment.getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.containerView,localfragment).addToBackStack(null).commit();

            }
        });

    }


    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}

//--------------------------------------Custom Adapter1 End---------------------------------------------------------------------------------------//

//--------------------------------------Custom Adapter2 Start---------------------------------------------------------------------------------------//

/**
 * Provide views to RecyclerView with data from mDataSet.
 * This Adapter is for first recyclerView
 */
class CustomAdapter2 extends RecyclerView.Adapter<CustomAdapter2.ViewHolder> {
    private static final String TAG = "CustomAdapter1";

    private ArrayList<Person> mDataSet = new ArrayList<Person>();
    Fragment fragment;

    // BEGIN_INCLUDE(recyclerViewSampleViewHolder)
    /**
     * Provide a reference to the type of views that you are using (custom ViewHolder)
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView textView_name;
        private final TextView textView_status;

        public ViewHolder(View v) {
            super(v);
            // Define click listener for the ViewHolder's View.
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "Element " + getPosition() + " clicked.");
                }
            });
            textView_name = (TextView) v.findViewById(R.id.textview_person_name_id);
            textView_status = (TextView) v.findViewById(R.id.textView_person_status_id);
        }

        public TextView getTextView_name() {
            return textView_name;
        }

        public TextView getTextView_status() {
            return textView_status;
        }
    }
    // END_INCLUDE(recyclerViewSampleViewHolder)

    /**
     * Initialize the dataset of the Adapter.
     *
     * @param dataSet String[] containing the data to populate views to be used by RecyclerView.
     */
    public CustomAdapter2(ArrayList<Person> dataSet, Fragment frag) {
        mDataSet = dataSet;
        this.fragment = frag;
    }

    // BEGIN_INCLUDE(recyclerViewOnCreateViewHolder)
    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Create a new view.
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.people_detail_layout, viewGroup, false);


        return new ViewHolder(v);
    }
    // END_INCLUDE(recyclerViewOnCreateViewHolder)

    // BEGIN_INCLUDE(recyclerViewOnBindViewHolder)
    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Log.d(TAG, "Element " + position + " set.");


        // Get element from your dataset at this position and replace the contents of the view
        // with that element
        viewHolder.getTextView_name().setText(mDataSet.get(position).getName());
        viewHolder.getTextView_status().setText(mDataSet.get(position).getStatus());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(),"first recycler view ", Toast.LENGTH_SHORT).show();

                Fragment localfragment = new list_item_Fragment_detail();

                Bundle args = new Bundle();
                args.putString("data",mDataSet.get(position).getName());
                localfragment.setArguments(args);

                FragmentTransaction fragmentTransaction = fragment.getActivity().getSupportFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.containerView,localfragment).addToBackStack(null).commit();

            }
        });

    }
    // END_INCLUDE(recyclerViewOnBindViewHolder)

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}

//--------------------------------------Custom Adapter2 End---------------------------------------------------------------------------------------//


