package com.example.maninder.WishyHub;

import android.*;
import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.widget.Toast;

/**
 * Created by root on 2/26/17.
 */
@RequiresApi(api = Build.VERSION_CODES.M)
public class FingerPrintHandler extends FingerprintManager.AuthenticationCallback{
    private Context myContext;

    FingerPrintHandler(Context context) {
        myContext = context;
    }
    public void startAunthantication(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject crypto) {
        CancellationSignal cancel = new CancellationSignal();

        if(ActivityCompat.checkSelfPermission(myContext, Manifest.permission.USE_FINGERPRINT) == PackageManager.PERMISSION_GRANTED) {
            fingerprintManager.authenticate(crypto, cancel,0, this,null);
        }

    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        super.onAuthenticationSucceeded(result);
        showToast(" Aunthentcation succeed");
        myContext.startActivity(new Intent(myContext, Main_Navigation_Activity.class));

    }

    @Override
    public void onAuthenticationFailed() {
        super.onAuthenticationFailed();
        showToast(" Aunthentcation failed");
    }

    public void showToast(String string) {
        Toast.makeText(myContext, string, Toast.LENGTH_LONG)
                .show();
    }
}
