package com.example.maninder.WishyHub;

/**
 * Created by root on 7/21/16.
 */


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;



public class TabFragment1 extends Fragment implements View.OnClickListener {

    private EditText number;
    private int doubledValue = 0;
    private TextView result;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab_fragment_1, container, false);


        number = (EditText) rootView.findViewById(R.id.number_input_tab1);
        result = (TextView) rootView.findViewById(R.id.result_tab1);

        Button show_calculation = (Button) rootView.findViewById(R.id.button_show_calculation_tab1);
        show_calculation.setOnClickListener(this);


        return rootView;
    }

    public void fragment1_click() {


        new Thread(new Runnable() {
            public void run() {

                try{
                    URL url = new URL("http://98.248.201.20:8080/web_server/double");
                    URLConnection connection = url.openConnection();

                    String inputString = number.getText().toString();
                    //inputString = URLEncoder.encode(inputString, "UTF-8");

                    Log.d("inputString", inputString);

                    connection.setDoOutput(true);
                    OutputStreamWriter out = new OutputStreamWriter(connection.getOutputStream());
                    out.write(inputString);
                    out.close();

                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    String returnString="";


                    while ((returnString = in.readLine()) != null)
                    {
                        doubledValue = Integer.parseInt(returnString);
                    }


                    in.close();


                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            result.setText(Integer.toString(doubledValue));
                        }
                    });

                }catch(Exception e)
                {
                    Log.d("Exception",e.toString());
                }

            }
        }).start();

    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_show_calculation_tab1:


                fragment1_click();

                break;

        }
    }
}