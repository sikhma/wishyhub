package com.example.maninder.WishyHub;

/**
 * Created by root on 11/4/16.
 */

public class CustomException extends Exception {

    public  CustomException() {
        super("Global Exception occur hard to identify");
    }

    public CustomException(String message) {
        super(message);
    }
}
