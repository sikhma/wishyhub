package com.example.maninder.WishyHub;


import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;


/**

 */
public class list_item_Fragment_detail extends Fragment implements  View.OnClickListener{
    ImageButton imageButton_play;
    ImageButton imageButton_stop;
    MediaPlayer mPlayer;
    String song_uri;
    String song_size= null;
    String song_length;
    TextView textView_size;
    TextView textView_duration;
    TextView textView_detail, songProgress;
    String song_name;
    SeekBar seekBar;
    Handler mHandler;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_list_item_detail, container, false);

         song_uri = getArguments().getString("uri");
         song_name = getArguments().getString("name");
        song_size = getArguments().getString("size");
        song_length = getArguments().getString("length");

        seekBar = (SeekBar) rootView.findViewById(R.id.id_seekBar);
        songProgress = (TextView) rootView.findViewById(R.id.id_textView_progress);



        seekBar.setOnTouchListener(new View.OnTouchListener() {public boolean onTouch(View v, MotionEvent event) {
            mPlayer.seekTo(seekBar.getProgress());
            return false; }
        });

         textView_detail = (TextView) rootView.findViewById(R.id.TextView_listView_item_fragment);
         textView_size = (TextView) rootView.findViewById(R.id.TextView_song_size);
         textView_duration = (TextView) rootView.findViewById(R.id.TextView_song_duration);
        imageButton_play = (ImageButton) rootView.findViewById(R.id.imageButton_play_song);
        imageButton_stop = (ImageButton) rootView.findViewById(R.id.imageButton2_stop_song);


        imageButton_play.setOnClickListener(this);
        imageButton_stop.setOnClickListener(this);

        textView_detail.setText(song_name);
        textView_size.setText(convert_bit_to_mb(song_size));
        textView_duration.setText(convert_milisecond_to_minuteSecond(song_length));


        return rootView;
    }


    public String convert_milisecond_to_minuteSecond(String duration) {
        String out = "";
        long dur = Long.parseLong(duration);
        String seconds = String.valueOf((dur % 60000) / 1000);

        Log.v("seconds", seconds);
        String minutes = String.valueOf(dur / 60000);
        out = minutes + ":" + seconds;
        if (seconds.length() == 1) {
          //  textView_duration.setText("0" + minutes + ":0" + seconds);
        }else {
         //   textView_duration.setText("0" + minutes + ":" + seconds);
        }

        return minutes + " : " + seconds;
    }

    public String convert_bit_to_mb(String duration) {
        String out = "";
        if(duration == null) {
            return  out;
        }
        long dur = Long.parseLong(duration);

        long fileSizeInMB = (dur / 1024)/1024;

        out = String.valueOf(fileSizeInMB);


        return out + " mb";
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageButton_play_song:
                play_song(v);
                imageButton_play.setImageResource(R.drawable.ic_media_pause);
                seekBar.setMax(mPlayer.getDuration());

                  mHandler = new Handler();
//Make sure you update Seekbar on UI thread
                getActivity().runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        if(mPlayer != null){
                            int mCurrentPosition = mPlayer.getCurrentPosition() / 1000;
                            songProgress.setText(Integer.toString(mCurrentPosition));
                            seekBar.setProgress(mCurrentPosition);
                            seekBar.refreshDrawableState();
                        }
                        mHandler.postDelayed(this, 1000);
                    }
                });
                break;
            case R.id.imageButton2_stop_song:
                if(mPlayer!=null && mPlayer.isPlaying()){
                    mPlayer.stop();
                    Toast.makeText(getActivity(), "song stop", Toast.LENGTH_LONG).show();
                    imageButton_play.setImageResource(R.drawable.ic_media_play);
                }
                break;
        }
    }

    public void play_song(View view) {
        if(mPlayer!=null && mPlayer.isPlaying()) {
            mPlayer.pause();
        } else {


            Uri myUri1 = Uri.parse(song_uri);
            String exception = "Wrong Uri";
            mPlayer = new MediaPlayer();
            mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            try {
                mPlayer.setDataSource(getActivity(), myUri1);
            } catch (IllegalArgumentException e) {
                Toast.makeText(getActivity(), exception, Toast.LENGTH_LONG).show();
            } catch (SecurityException e) {
                Toast.makeText(getActivity(), exception, Toast.LENGTH_LONG).show();
            } catch (IllegalStateException e) {
                Toast.makeText(getActivity(), exception, Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                mPlayer.prepare();

            } catch (IllegalStateException e) {
                Toast.makeText(getActivity(), exception, Toast.LENGTH_LONG).show();
            } catch (IOException e) {
                Toast.makeText(getActivity(), exception, Toast.LENGTH_LONG).show();
            }
            mPlayer.start();
            Toast.makeText(getActivity(), "song play", Toast.LENGTH_LONG).show();
        }

    }
}
