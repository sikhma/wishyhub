package com.example.maninder.WishyHub;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.lang.Math;



/**
 * Created by root on 11/25/16.
 */

public class Distance_measure_fragment extends Fragment implements View.OnClickListener {
    EditText from_lang;
    EditText to_lang;
    EditText from_lat;
    EditText to_lat;
    TextView result;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_distance_measure_layout, container, false);

         from_lang = (EditText) rootView.findViewById(R.id.editText_frag_distance_from_lang);
         to_lang = (EditText) rootView.findViewById(R.id.editText__frag_distance_to_lang);
         from_lat = (EditText) rootView.findViewById(R.id.editText_frag_distance_from_lat);
         to_lat = (EditText) rootView.findViewById(R.id.editText__frag_distance_to_lat);

        Button distance_measure = (Button) rootView.findViewById(R.id.button_fragment_distance_result);
        result =  (TextView) rootView.findViewById(R.id.textView_fragment_distance_result);
        distance_measure.setOnClickListener(this);


        return rootView;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_fragment_distance_result:

               double distance_result=  calculte_distance(Double.parseDouble(from_lat.getText().toString()), Double.parseDouble(from_lang.getText().toString()),
                        Double.parseDouble(to_lat.getText().toString()), Double.parseDouble(to_lang.getText().toString()));

                result.setText(String.valueOf(distance_result + " mil"));

        }
    }

    public double calculte_distance(double from_lat,double from_lang, double to_lat, double to_lang) {
        double result = 0.0;


        double deg_to_radian = Math.PI / 180.0;

        double lang_diff = (from_lang - to_lang) * deg_to_radian;
        double lat_diff = (from_lat - to_lat) * deg_to_radian;

        double a = Math.pow(Math.sin(lat_diff/2.0), 2) + Math.cos(from_lat*deg_to_radian) * Math.cos(to_lat*deg_to_radian) * Math.pow(Math.sin(lang_diff/2.0), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

        result = 3956 * c; // in mile if u want in kilometer use 6367 * c;


        return Math.round(result);
    }
}
