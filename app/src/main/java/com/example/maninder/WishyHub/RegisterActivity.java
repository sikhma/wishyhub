package com.example.maninder.WishyHub;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.support.design.widget.Snackbar;
public class RegisterActivity extends AppCompatActivity {

    private TextView status;
    private EditText username = null;
    private EditText password1, password2 = null;

    private String server_status = "server not responding";

    public void set_status(String st) {
        server_status = st;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_resgister);

        Button register = (Button) findViewById(R.id.register_button);
        username = (EditText) findViewById(R.id.username_register_input);
        password1 = (EditText) findViewById(R.id.password_register_input);
        password2 = (EditText) findViewById(R.id.confirm_pass_input);

        status = (TextView) findViewById(R.id.status_register);

        status.setText("Not success");

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                register_execute(view);
            }
        });
    }

    public void register_execute(View view) {


        String user_name_local = username.getText().toString();
        String password_1 = password1.getText().toString();
        String password_2 = password2.getText().toString();


        hide_keyboard();  // this function will hide keyboard after click on register button


        if (user_name_local.equals("")) {

            Snackbar.make(view, "Enter the user name", Snackbar.LENGTH_LONG).setAction("Action", null).show();


        } else if (!user_name_local.equals("") && password_1.equals("")) { // just to check user name availble or not

            new Register_controller(this, status).execute(user_name_local, password_1);

            //server_status =  new Universal_class().get_status_for_register_activity();  // getting statsus from Universal-class from server

           // server_status = Universal_class.status_for_register_activity;

          //  Snackbar.make(view,server_status, Snackbar.LENGTH_LONG).setAction("Action", null).show();

        } else if (!user_name_local.equals("") && password_1.equals(password_2) && !password_1.equals("")) {
            // checking username from server side


            new Register_controller(this, status).execute(user_name_local, password_1);

           // server_status =  new Universal_class().get_status_for_register_activity();  // getting statsus from Universal-class from server

       //     server_status = Universal_class.status_for_register_activity;

           // Snackbar.make(view,server_status, Snackbar.LENGTH_LONG).setAction("Action", null).show();

        } else if (password_1.equals("") && password_2.equals("")) { // condition if user did not enter password

            Snackbar.make(view, "password is empty", Snackbar.LENGTH_LONG).setAction("Action", null).show();

        } else if (!password_1.equals(password_2)) {  // condition if user both password dont match
            Snackbar.make(view, "password not match", Snackbar.LENGTH_LONG).setAction("Action", null).show();
        }


    }

    public void show_dialog_box(String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setMessage(msg);
        alertDialog.setCancelable(true);
        alertDialog.create();
        alertDialog.show();
    }


    public void hide_keyboard() {

        try {
            InputMethodManager input = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

            input.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        } catch (Exception e) {

        }
    }

}