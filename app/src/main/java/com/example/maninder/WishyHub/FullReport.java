package com.example.maninder.WishyHub;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class FullReport extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_report);

        String value = getIntent().getExtras().getString("report");

        TextView textView = (TextView) findViewById(R.id.textView_fullReport);

        textView.setText(value);

    }
}
