package com.example.maninder.WishyHub;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by root on 10/10/16.
 */

public class ViewStuffBeforePost extends Fragment {

    TextView title, detail;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_view_sell_post, container, false);

        String local_title = getArguments().getString("title");
        String local_price = getArguments().getString("price");
        String local_detail = getArguments().getString("detail");
        Uri picture = Uri.parse(getArguments().getString("picture"));


        title = (TextView) rootView.findViewById(R.id.textview_fragment_viewsell_post_title);
        detail = (TextView) rootView.findViewById(R.id.textview_fragment_viewsell_post_discription);
        ImageView imageView = (ImageView) rootView.findViewById(R.id.imageView_fragment_viewsell_post_description);

        imageView.setImageURI(picture);
       // imageView.setImageBitmap(BitmapFactory.decodeFile(picture));

        title.setText(local_title + " ( " +local_price + " $ ) ");
        detail.setText(local_detail);

        return rootView;
    }
}
