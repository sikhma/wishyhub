package com.example.maninder.WishyHub;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by root on 9/30/16.
 */


class MovieAdapter extends ArrayAdapter {


    private int resource;
    private LayoutInflater inflater;

    private List<Person> myPersonlist = new ArrayList<Person>();


    public MovieAdapter(Context context, int resource, List<Person> object) {
        super(context, resource, object);

        myPersonlist = object;
        this.resource = resource;
        inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {

            convertView = inflater.inflate(R.layout.layout_content_list_item_design, null);

        }

        ImageView image;
        TextView name;
        TextView status;

        image = (ImageView)convertView.findViewById(R.id.list_view_imageView);
        name = (TextView) convertView.findViewById(R.id.list_view_data);


        name.setText(myPersonlist.get(position).getName());


        return convertView;
    }


}
