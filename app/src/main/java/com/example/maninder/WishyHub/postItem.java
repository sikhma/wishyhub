package com.example.maninder.WishyHub;

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;



/**
 * Created by root on 10/10/16.
 * This class is crested for add your post example sell your stuff
 */

public class postItem extends Fragment implements  View.OnClickListener {

    EditText title, price, detail;
    private static final int RESULT_LOAD_IMAGE = 1;
    private static final int REQUEST_IMAGE_CAPTURE = 2;
    private static final int REQUEST_TAKE_PHOTO = 2;
    private Uri global_image_uri;
    private ImageView imageView;
    private ProgressDialog pd;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_postitem, container, false);

        Button viewItem = (Button) rootView.findViewById(R.id.button_fragment_postitem_view);
        viewItem.setOnClickListener(this);
        imageView = (ImageView) rootView.findViewById(R.id.imageView_fragment_postitem);

        Button addPhoto = (Button) rootView.findViewById(R.id.button_fragment_postitem_addphoto);
        Button postPhoto = (Button) rootView.findViewById(R.id.button_fragment_postitem_post);
        addPhoto.setOnClickListener(this);
        postPhoto.setOnClickListener(this);
        title = (EditText) rootView.findViewById(R.id.editText_fragment_postitem_title);
        price = (EditText) rootView.findViewById(R.id.editText_fragment_postitem_price);
        detail = (EditText) rootView.findViewById(R.id.editText_fragment_postitem_detail);


        try {
            imageView.setImageResource(0);
            DownloadImageTask dt = (DownloadImageTask) new DownloadImageTask(imageView, getActivity()).execute("http://www.wishyhub.com/profile/img/cat.jpg");
            if(imageView.getDrawable() == null) {
                show_toast("fail to fetch image");
            }


            if (dt == null) {  // this is not correct need to look at it
                throw new CustomException("fetching from Server failed");
            }

            // throw new CustomException ("fetching from Server failed");
        } catch (CustomException m) {
            //  Toast.makeText(getContext(), "image fetching from Server failed", Toast.LENGTH_SHORT).show();
            m.printStackTrace();
        }
        return rootView;
    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_fragment_postitem_view:
                Fragment localfragment = new ViewStuffBeforePost();

                Bundle args = new Bundle();

                if( TextUtils.isEmpty(global_image_uri.toString())) {
                    break;

                }

                if( TextUtils.isEmpty(title.getText().toString())) {
                    title.setText("");
                }
                if( TextUtils.isEmpty(price.getText().toString())) {
                    price.setText("");
                }
                if( TextUtils.isEmpty(detail.getText().toString())) {
                    detail.setText("");
                }


                args.putString("title", title.getText().toString());
                args.putString("price", price.getText().toString());
                args.putString("detail", detail.getText().toString());

                args.putString("picture", global_image_uri.toString());

                localfragment.setArguments(args);

                FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();

                fragmentTransaction.replace(R.id.containerView, localfragment).addToBackStack(null).commit(); // addtoBackStack function for when press back button it comes to previous screen

                break;
            case R.id.button_fragment_postitem_post:
                // post item to server and show notification

                notification();


                break;
            case R.id.button_fragment_postitem_addphoto:
                // Add photo from gallery or take pic
                //To improive code i can use sitch to choose option from dialog
                global_image_uri = null;
                Camera_handler camera_handler = new Camera_handler();

                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.custom_dialog_box);
                dialog.setTitle(" Add photo ");

                Button btnExit = (Button) dialog.findViewById(R.id.btnExit);
                btnExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.findViewById(R.id.btnChoosePath)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Camera_handler camera_handler = new Camera_handler();
                                camera_handler.setContext(getActivity());
                                camera_handler.activeGallery();
                                Toast.makeText(getContext(), "choose photo", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        });
                dialog.findViewById(R.id.btnTakePhoto)
                        .setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                // activeTakePhoto();
                                //Toast.makeText(getContext(), " photo Taken", Toast.LENGTH_SHORT).show();
                                Camera_handler camera_handler = new Camera_handler();
                                camera_handler.setContext(getActivity());
                                camera_handler.dispatchTakePictureIntent();
                                dialog.dismiss();
                            }
                        });

                dialog.show();

                // add photo end
                break;
        }

    }

    private void notification() {
        NotificationCompat.Builder builder = (NotificationCompat.Builder) new NotificationCompat.Builder(getActivity())
                .setSmallIcon(R.drawable.cat)
                .setContentTitle("WishyHub Notification")
                .setContentText("post notification");

        if (global_image_uri != null) {
            Camera_handler camera_handler = new Camera_handler();

            NotificationCompat.BigPictureStyle bigPictureStyle = new NotificationCompat.BigPictureStyle();
            bigPictureStyle.bigPicture(BitmapFactory.decodeFile(camera_handler.get_path_from_uri(global_image_uri)));


            bigPictureStyle.setBigContentTitle("Maninderpal Singh");
            bigPictureStyle.setSummaryText("Item has posted successfully");
            bigPictureStyle.bigLargeIcon(BitmapFactory.decodeFile(camera_handler.get_path_from_uri(global_image_uri)));
            builder.setStyle(bigPictureStyle);
        }
        Intent notificationIntent = new Intent(getActivity(), postItem.class);
        PendingIntent contentIntent = PendingIntent.getActivity(getActivity(), 0, notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(contentIntent);

        // Add as notification
        NotificationManager manager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, builder.build());
    }




    private void show_toast(String message) {
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }




/*
* class for download imagefrom server
 */

}