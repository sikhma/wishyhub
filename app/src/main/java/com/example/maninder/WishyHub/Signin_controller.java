package com.example.maninder.WishyHub;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.TextView;

public class Signin_controller extends AsyncTask<String,Void,String>{
    private String  error_status;
    private Context context;
    private int byGetOrPost = 0;
    private String err = "error";
    private String status = " ";
    private ArrayList<String> list = new ArrayList<>(2);

    private String username = "";

    private String domainname;


    //flag 0 means get and 1 means post.(By default it is get.)
    public Signin_controller(Context context, String statusField, String err_status, int flag) {
        this.context = context;
      //  this.roleField = roleField;
        byGetOrPost = flag;
        this.error_status= err_status;
        domainname =  this.context.getResources().getString(R.string.domain_name);
    }

    protected void onPreExecute(){

    }

    @Override
    protected String doInBackground(String... arg0) {


        if(byGetOrPost == 0){ //means by Get Method

            try{
                String username = arg0[0];
                String password = arg0[1];

                String link = domainname + "/project/login.php";

                URL url = new URL(link);
                HttpClient client = new DefaultHttpClient();
                HttpGet request = new HttpGet();
                request.setURI(new URI(link));
                HttpResponse response = client.execute(request);
                BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer sb = new StringBuffer("");
                String line="";

                while ((line = in.readLine()) != null) {
                    sb.append(line);
                    break;
                }
                in.close();

                System.out.print("\n\n THIS IS STRING "+ sb.toString() +"\n\n");
                // return sb.toString();

                err = sb.toString();
                return "";
            }

            catch(Exception e){
                err =  e.getMessage().toString();
                // return new String("Exception: " + e.getMessage());
                return "";
            }
        }
        else{
            try{
                username = arg0[0];
                String password = arg0[1];


                String link = domainname + "/project/login.php";

                String data  = URLEncoder.encode("username", "UTF-8") + "=" + URLEncoder.encode(username, "UTF-8");
                data += "&" + URLEncoder.encode("password", "UTF-8") + "=" + URLEncoder.encode(password, "UTF-8");

                URL url = new URL(link);
                URLConnection conn = url.openConnection();

                conn.setDoOutput(true);
                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());

                wr.write( data );
                wr.flush();

                BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                StringBuilder sb = new StringBuilder();
                String line = null;

                // Read Server Response
                while((line = reader.readLine()) != null)
                {
                    sb.append(line);
                    break;
                }

                err = sb.toString();
                // return sb.toString();

                Decode(sb);

                return "";
            }
            catch(Exception e){
                err =  e.getMessage().toString();

                // return new String("Exception: " + e.getMessage());
                return "";
            }
        }
    }

    public  void Decode(CharSequence result) throws JSONException {
        // TODO decode the JSON CharSequence (result)
        // get the 'posts' section from the JSON string
        JSONObject posts = null;
        try {
            posts = new JSONObject(result.toString()).getJSONObject("posts");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // Get 'fname' & 'lname' from 'posts'
      //  Log.i(JSONParser.class.getName(),"username is:"+posts.getString("username"));
        //Log.i(JSONParser.class.getName(), "password is:" + posts.getString("password"));

           // status = " "+posts.getString("fname");

            if (posts == null) {
                status = " "+posts.getString("status");
            }

        Iterator<?> keys = posts.keys();  // use iterator to get json object


        while (keys.hasNext()) {
            String key = (String)keys.next();


                list.add(posts.get(key).toString());   // add item from json array in list first is error and 2nd is status

        }

        err = list.get(0);
        status = list.get(1);


    }

    /* Helper function. put it in the same page, or in a library */

    @Override
    protected void onPostExecute(String result){

        this.error_status = err;

        if(status.equals("success"))
        {
            Intent intent = new Intent(context, Main_Navigation_Activity.class);
            intent.putExtra("title", username );
            context.startActivity(intent);
        } else {

            show_alert_box(status);

        }

    }

    public void show_alert_box(String alert) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setMessage(alert);
        builder1.setCancelable(true);
        builder1.create();
        builder1.show();
    }

}

